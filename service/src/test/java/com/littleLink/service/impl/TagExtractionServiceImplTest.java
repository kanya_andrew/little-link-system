package com.littleLink.service.impl;

import com.littleLink.model.Tag;
import com.littleLink.service.TagExtractionService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class TagExtractionServiceImplTest {
    private static final Logger LOGGER = LogManager.getLogger(TagExtractionServiceImplTest.class);
    TagExtractionService tagExtractionService;

    @Before
    public void init (){
        tagExtractionService = new TagExtractionServiceImpl();
    }
    @Test
    public void testExtractTagsFromString() {
        String stringToTest = "WWF, has, been, protecting";
        Set<Tag> expectedTagSet = new HashSet<>();
        Tag tag1 = new Tag();
        tag1.setTitle("WWF");
        expectedTagSet.add(tag1);
        Tag tag2 = new Tag();
        tag2.setTitle("has");
        expectedTagSet.add(tag2);
        Tag tag3 = new Tag();
        tag3.setTitle("been");
        expectedTagSet.add(tag3);
        Tag tag4 = new Tag();
        tag4.setTitle("protecting");
        expectedTagSet.add(tag4);
        LOGGER.info("testExtractTagsFromString() expected set " + expectedTagSet);
        Set<Tag> actualTagSet = tagExtractionService.extractTagsFromString(stringToTest);
        LOGGER.info("testExtractTagsFromString() actual set " + actualTagSet);
        Assert.assertEquals(expectedTagSet,actualTagSet);
    }

    @Test
    public void testExtractTagsFromStringNotUnique() {
        String stringToTest = "WWF, has, been, been , protecting";
        Set<Tag> expectedTagSet = new HashSet<>();
        Tag tag1 = new Tag();
        tag1.setTitle("WWF");
        expectedTagSet.add(tag1);
        Tag tag2 = new Tag();
        tag2.setTitle("has");
        expectedTagSet.add(tag2);
        Tag tag3 = new Tag();
        tag3.setTitle("been");
        expectedTagSet.add(tag3);
        Tag tag4 = new Tag();
        tag4.setTitle("protecting");
        expectedTagSet.add(tag4);
        LOGGER.info("testExtractTagsFromStringNotUnique() expected set " + expectedTagSet);
        Set<Tag> actualTagSet = tagExtractionService.extractTagsFromString(stringToTest);
        LOGGER.info("testExtractTagsFromStringNotUnique() actual set " + actualTagSet);
        Assert.assertEquals(expectedTagSet,actualTagSet);
    }

}