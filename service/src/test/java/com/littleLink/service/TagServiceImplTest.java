package com.littleLink.service;

import com.littleLink.dao.TagDao;
import com.littleLink.model.Tag;
import com.littleLink.service.impl.TagServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TagServiceImplTest {

    private static final int TAG_TO_TEST_ID = 1;

    @InjectMocks
    TagServiceImpl tagService;

    @Mock
    TagDao tagDao;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetTagById() {
        Tag expectedTag = createTagToTest();
        when(tagDao.getTagById(anyInt())).thenReturn(expectedTag);

        Tag actualTag = tagService.getTagById(TAG_TO_TEST_ID);

        verify(tagDao).getTagById(anyInt());
        Assert.assertEquals(expectedTag, actualTag);
    }

    @Test
    public void testSaveTag() {

        when(tagDao.saveTag(any(Tag.class))).thenReturn(1);
        Tag tag = createTagToTest();

        tagService.saveTag(tag);

        verify(tagDao).saveTag(tag);
    }

    private Tag createTagToTest() {
        Tag tag = new Tag();
        tag.setId(TAG_TO_TEST_ID);
        tag.setTitle("stackoverflow");
        return tag;
    }

}