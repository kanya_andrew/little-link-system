package com.littleLink.service;

import com.littleLink.dao.UrlDao;
import com.littleLink.model.Url;
import com.littleLink.service.impl.UrlServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class UrlServiceImplTest {

    private static final int URL_TO_TEST_ID = 1;

    @InjectMocks
    UrlServiceImpl urlService;

    @Mock
    UrlDao urlDao;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void testGetUrlById() {
        Url expectedUrl = createUrlToTest();
        when(urlDao.getUrlById(anyInt())).thenReturn(expectedUrl);

        Url actualUrl = urlDao.getUrlById(URL_TO_TEST_ID);

        verify(urlDao).getUrlById(anyInt());
        Assert.assertEquals(expectedUrl, actualUrl);
    }

    @Test
    public void testSaveUrl() {

        when(urlDao.saveUrl(any(Url.class))).thenReturn(1);
        Url url = createUrlToTest();

        urlService.saveUrl(url);

        verify(urlDao).saveUrl(url);
    }

    private Url createUrlToTest() {
        Url url = new Url();
        url.setId(URL_TO_TEST_ID);
        url.setTitle("stackoverflow");
        url.setCounter(1);
        url.setDescription("stackoverflow.com");
        url.setFullPath("http://stackoverflow.com/questions/25487116/log4j2-configuration-no-log4j2-configuration-file-found");
        url.setReducedPath("http://little-link.com/safre524");
        return url;
    }

}