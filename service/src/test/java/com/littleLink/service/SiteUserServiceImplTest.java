package com.littleLink.service;

import com.littleLink.dao.SiteUserDao;
import com.littleLink.model.SiteUser;
import com.littleLink.service.impl.SiteUserServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SiteUserServiceImplTest {

    private static final int SITE_USER_TO_TEST_ID = 1;
    @InjectMocks
    SiteUserServiceImpl siteUserService;

    @Mock
    SiteUserDao siteUserDao;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetSiteUserById() {
        SiteUser expectedSiteUser = createSiteUserToTest();
        when(siteUserDao.getSiteUserById(anyInt())).thenReturn(expectedSiteUser);

        SiteUser actualSiteUser = siteUserService.getSiteUserById(SITE_USER_TO_TEST_ID);

        verify(siteUserDao).getSiteUserById(anyInt());
        Assert.assertEquals(expectedSiteUser, actualSiteUser);

    }

    @Test
    public void testSaveSiteUser() {
        SiteUser siteUser = createSiteUserToTest();
        when(siteUserDao.saveSiteUser(any(SiteUser.class))).thenReturn(1);

        siteUserService.saveSiteUser(siteUser);

        verify(siteUserDao).saveSiteUser(any(SiteUser.class));
    }

    private SiteUser createSiteUserToTest() {
        SiteUser siteUser = new SiteUser();
        siteUser.setId(SITE_USER_TO_TEST_ID);
        siteUser.setLogin("andrew");
        siteUser.setPassword("password");
        siteUser.setEmail("andrew@email.com");
        siteUser.setDeleted(false);
        return siteUser;
    }

}