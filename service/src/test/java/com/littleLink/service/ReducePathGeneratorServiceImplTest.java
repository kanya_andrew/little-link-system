package com.littleLink.service;

import com.littleLink.service.impl.ReducePathGeneratorServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ReducePathGeneratorServiceImplTest {

    private static final Logger LOGGER = LogManager.getLogger(ReducePathGeneratorServiceImplTest.class);
    private static final int CODE_LENGTH = 7;


    ReducePathGeneratorService reducePathGeneratorService;

    @Before
    public void init(){
        reducePathGeneratorService = new ReducePathGeneratorServiceImpl();
    }
    @Test
    public void testGenerateReducedPathLength() {
        String actualString = reducePathGeneratorService.generateReducedPath();
        LOGGER.info(actualString);
        Assert.assertEquals(CODE_LENGTH, actualString.length());
    }

    @Test
    public void testGenerateReducedPathDifferent() {
        String actualString1 = reducePathGeneratorService.generateReducedPath();
        String actualString2 = reducePathGeneratorService.generateReducedPath();
        LOGGER.info(actualString1);
        LOGGER.info(actualString2);
        Assert.assertFalse(actualString1.equals(actualString2));
    }
}