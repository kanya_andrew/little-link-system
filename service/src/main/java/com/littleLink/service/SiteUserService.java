package com.littleLink.service;

import com.littleLink.model.SiteUser;

public interface SiteUserService {

    SiteUser getSiteUserById(int siteUserId);

    int saveSiteUser(SiteUser siteUser);

    SiteUser getSiteUserByLogin(String userLogin);
}
