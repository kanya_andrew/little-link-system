package com.littleLink.service;

public interface ReducePathGeneratorService {

    String generateReducedPath();
}
