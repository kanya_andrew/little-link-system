package com.littleLink.service;

import com.littleLink.model.SiteUser;

public interface RegistrationService {

    int bookNewUserAccount(SiteUser siteUser);
}
