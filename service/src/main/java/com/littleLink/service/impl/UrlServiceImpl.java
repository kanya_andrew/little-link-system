package com.littleLink.service.impl;

import com.littleLink.dao.UrlDao;
import com.littleLink.model.SiteUser;
import com.littleLink.model.Url;
import com.littleLink.service.UrlService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UrlServiceImpl implements UrlService {

    private static final Logger LOGGER = LogManager.getLogger(UrlServiceImpl.class);

    @Autowired
    UrlDao urlDao;

    @Override
    public Url getUrlById(int urlId) {
        LOGGER.info("getUrlById() started");
        return urlDao.getUrlById(urlId);
    }

    @Override
    public int saveUrl(Url url) {
        LOGGER.info("saveUrl() started");
        return urlDao.saveUrl(url);
    }

    @Override
    public Url getUrlByReducedUrl(String shortUrl) {
        LOGGER.info("getUrlByReducedUrl() started");
        return urlDao.getUrlByReducedUrl(shortUrl);
    }

    @Override
    public List<Url> getUserUrlList(SiteUser siteUser) {
        LOGGER.info("getUserUrlList() started");
        return urlDao.getUserUrlList(siteUser);
    }

    @Override
    public void updateUrl(Url url) {
        LOGGER.info("updateUrl() started");
        urlDao.updateUrl(url);
    }

}
