package com.littleLink.service.impl;

import com.littleLink.dao.TagDao;
import com.littleLink.model.Tag;
import com.littleLink.service.TagService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class TagServiceImpl implements TagService{

    private static final Logger LOGGER = LogManager.getLogger(TagServiceImpl.class);

    @Autowired
    TagDao tagDao;

    @Override
    public Tag getTagById(int tagId) {
        LOGGER.info("getTagById() started");
        return tagDao.getTagById(tagId);
    }

    @Override
    public int saveTag(Tag tag) {
        LOGGER.info("saveTag() started");
        return tagDao.saveTag(tag);
    }

    @Override
    public void saveListOfTags(Set<Tag> tagSet) {
        LOGGER.info("saveTag() started");
        tagDao.saveListOfTags(tagSet);
    }

    @Override
    public Tag getTagByTitle(String title) {
        LOGGER.info("getTagByTitle() started");
        return tagDao.getTagByTitle(title);
    }
}
