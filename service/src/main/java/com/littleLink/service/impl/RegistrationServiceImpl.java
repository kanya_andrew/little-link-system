package com.littleLink.service.impl;

import com.littleLink.dao.SiteUserDao;
import com.littleLink.model.Role;
import com.littleLink.model.SiteUser;
import com.littleLink.service.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RegistrationServiceImpl implements RegistrationService {

    @Autowired
    SiteUserDao siteUserDao;

    @Override
    public int bookNewUserAccount(SiteUser siteUser) {
        siteUser.setRole(Role.USER);
        siteUser.setDeleted(false);
        return siteUserDao.saveSiteUser(siteUser);
    }
}
