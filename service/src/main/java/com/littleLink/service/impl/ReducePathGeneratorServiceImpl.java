package com.littleLink.service.impl;

import com.littleLink.service.ReducePathGeneratorService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.stream.Collectors;

@Service
public class ReducePathGeneratorServiceImpl implements ReducePathGeneratorService{
    private static final Logger LOGGER = LogManager.getLogger(ReducePathGeneratorServiceImpl.class);
    private static final int CODE_LENGTH = 7;
    private static final String AVAILABLE_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";

    @Override
    public String generateReducedPath() {
        LOGGER.info("generateReducedPath() run");
        String string =  new SecureRandom()
                    .ints(CODE_LENGTH, 0, AVAILABLE_CHARS.length())
                    .mapToObj(AVAILABLE_CHARS::charAt)
                    .map(Object::toString)
                    .collect(Collectors.joining());
        LOGGER.info("random string is " + string);
        return string;
    }
}
