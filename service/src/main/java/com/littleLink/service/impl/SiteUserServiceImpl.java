package com.littleLink.service.impl;

import com.littleLink.dao.SiteUserDao;
import com.littleLink.model.SiteUser;
import com.littleLink.service.SiteUserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SiteUserServiceImpl implements SiteUserService {

    private static final Logger LOGGER = LogManager.getLogger(SiteUserServiceImpl.class);

    @Autowired
    SiteUserDao siteUserDao;

    @Override
    public SiteUser getSiteUserById(int siteUserId) {
        LOGGER.info("getSiteUserById() started");
        return siteUserDao.getSiteUserById(siteUserId);
    }

    @Override
    public int saveSiteUser(SiteUser siteUser) {
        LOGGER.info("saveSiteUser() started");
        return siteUserDao.saveSiteUser(siteUser);
    }

    @Override
    public SiteUser getSiteUserByLogin(String userLogin) {
        LOGGER.info("getSiteUserByLogin() started");
        return siteUserDao.getSiteUserByLogin(userLogin);
    }
}
