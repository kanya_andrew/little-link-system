package com.littleLink.service.impl;

import com.littleLink.model.Tag;
import com.littleLink.service.TagExtractionService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Service
public class TagExtractionServiceImpl implements TagExtractionService {
    private static final Logger LOGGER = LogManager.getLogger(ReducePathGeneratorServiceImpl.class);

    @Override
    public Set<Tag> extractTagsFromString(String stringOfTags) {
        LOGGER.info("extractTagsFromString() got string " + stringOfTags);
        String trimmedStringOfTags = stringOfTags.trim();
        String[] array = trimmedStringOfTags.split(",", -1);
        String[] trimmedArray = new String[array.length];

        for (int i = 0; i < array.length; i++) {
            trimmedArray[i] = array[i].trim();
        }
        Set<String> stringHashSet = new HashSet<>(Arrays.asList(trimmedArray));
        Set<Tag> tagSet = new HashSet<>();
        for (String string : stringHashSet) {
            Tag tag = new Tag();
            tag.setTitle(string);
            tagSet.add(tag);
        }
        LOGGER.info("extractTagsFromString() returned " + tagSet);
        return tagSet;
    }
}
