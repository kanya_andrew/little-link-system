package com.littleLink.service;

import com.littleLink.model.Tag;

import java.util.Set;

public interface TagExtractionService {

    Set<Tag> extractTagsFromString(String stringOfTags);
}
