package com.littleLink.dao;

import com.littleLink.model.SiteUser;

public interface SiteUserDao {

    SiteUser getSiteUserById(int siteUserId);

    int saveSiteUser(SiteUser siteUser);

    SiteUser getSiteUserByLogin(String userLogin);
}
