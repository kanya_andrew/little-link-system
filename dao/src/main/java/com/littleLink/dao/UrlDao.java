package com.littleLink.dao;

import com.littleLink.model.SiteUser;
import com.littleLink.model.Url;

import java.util.List;

public interface UrlDao {

    Url getUrlById(int urlId);

    int saveUrl(Url url);

    Url getUrlByReducedUrl(String shortUrl);

    List<Url> getUserUrlList(SiteUser siteUser);

    void updateUrl(Url url);
}
