package com.littleLink.dao.impl;

import com.littleLink.dao.SiteUserDao;
import com.littleLink.model.SiteUser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class SiteUserDaoImpl implements SiteUserDao {

    private static final Logger LOGGER = LogManager.getLogger(SiteUserDaoImpl.class);
    private static SessionFactory sessionFactory;

    public SiteUserDaoImpl(SessionFactory sessionFactory) {
        LOGGER.info("SiteUserDaoImpl initialized");
        LOGGER.info("sessionFactory is null " + (sessionFactory == null));
        SiteUserDaoImpl.sessionFactory = sessionFactory;
    }


    @Override
    public SiteUser getSiteUserById(int siteUserId) {
        LOGGER.info("getSiteUserById() started");
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        SiteUser siteUser = session.get(SiteUser.class, siteUserId);

        transaction.commit();
        session.close();
        return siteUser;
    }

    @Override
    public int saveSiteUser(SiteUser siteUser) {
        LOGGER.info("saveSiteUser() started");

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        LOGGER.info("siteUser to save = " + siteUser);
        session.save(siteUser);

        transaction.commit();
        session.close();
        return 1;
    }

    @Override
    public SiteUser getSiteUserByLogin(String userName) {
        LOGGER.info("getSiteUserByLogin() started");

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        LOGGER.info("userName to user = " + userName);
        String queryText = " from site_user where login like '%" + userName + "%'";
        Query query = session.createQuery(queryText);
        SiteUser siteUser = new SiteUser();
        List<SiteUser> siteUserList = query.list();
        transaction.commit();
        session.close();
        LOGGER.info("getSiteUserByLogin returned = " + siteUserList);
        if (siteUserList.size() > 0) {
            siteUser = siteUserList.get(0);
        }
        LOGGER.info("getSiteUserByLogin() got user from result = " + siteUser);
        return siteUser;
    }
}
