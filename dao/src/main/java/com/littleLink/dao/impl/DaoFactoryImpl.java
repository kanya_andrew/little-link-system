package com.littleLink.dao.impl;

import com.littleLink.dao.DaoFactory;
import com.littleLink.dao.SiteUserDao;
import com.littleLink.dao.TagDao;
import com.littleLink.dao.UrlDao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class DaoFactoryImpl implements DaoFactory{

    private static final Logger LOGGER = LogManager.getLogger(DaoFactoryImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public SiteUserDao getSiteUserDao() {
        LOGGER.info("getSiteUserDao() started");
        return new SiteUserDaoImpl(sessionFactory);
    }

    @Override
    public TagDao getTagDao() {
        LOGGER.info("getTagDao() started");
        return new TagDaoImpl(sessionFactory);
    }

    @Override
    public UrlDao getUrlDao() {
        LOGGER.info("getUrlDao() started");
        return new UrlDaoImpl(sessionFactory);
    }

}
