package com.littleLink.dao.impl;

import com.littleLink.dao.UrlDao;
import com.littleLink.model.SiteUser;
import com.littleLink.model.Tag;
import com.littleLink.model.Url;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class UrlDaoImpl implements UrlDao {

    private static final Logger LOGGER = LogManager.getLogger(DaoFactoryImpl.class);
    private static SessionFactory sessionFactory;


    public UrlDaoImpl(SessionFactory sessionFactory) {
        LOGGER.info("UrlDaoImpl initialized");
        LOGGER.info("sessionFactory is null " + (sessionFactory == null));
        UrlDaoImpl.sessionFactory = sessionFactory;
    }

    @Override
    public Url getUrlById(int urlId) {
        LOGGER.info("getUrlById() started");
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Url url = session.get(Url.class, urlId);

        transaction.commit();
        session.close();
        return url;
    }

    @Override
    public int saveUrl(Url url) {
        LOGGER.info("saveUrl() started");

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        url.getTagList().forEach(session::update);
        session.save(url);

        transaction.commit();
        session.close();
        return 1;
    }

    @Override
    public Url getUrlByReducedUrl(String shortUrl) {
        LOGGER.info("getUrlByReducedUrl() started");

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        LOGGER.info("shortUrl to getUrl = " + shortUrl);
        String queryText = " from url where reduced_path like '%" + shortUrl + "%'";
        Query query = session.createQuery(queryText);
        Url url = new Url();
        List<Url> urlList = query.list();
        transaction.commit();
        session.close();
        LOGGER.info("getUrlByReducedUrl returned = " + urlList);
        if (urlList.size() > 0) {
            url = urlList.get(0);
        }
        LOGGER.info("getUrlByReducedUrl() got url from result = " + url);

        return url;
    }

    @Override
    public List<Url> getUserUrlList(SiteUser siteUser) {
        LOGGER.info("getUserUrlSet() started");

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        LOGGER.info("siteUser for urlList = " + siteUser);
        String queryText = " from url where addedByUser.id =" + siteUser.getId();
        Query query = session.createQuery(queryText);
        List<Url> urlList = query.list();
        transaction.commit();
        session.close();
        LOGGER.info("getUserUrlSet returned = " + urlList);
        return urlList;
    }

    @Override
    public void updateUrl(Url url) {
        LOGGER.info("updateUrl() started");

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        url.getTagList().forEach(session::update);
        session.update(url);

        transaction.commit();
        session.close();
    }
}
