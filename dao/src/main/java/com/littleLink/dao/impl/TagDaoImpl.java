package com.littleLink.dao.impl;

import com.littleLink.dao.TagDao;
import com.littleLink.model.Tag;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class TagDaoImpl implements TagDao {

    private static final Logger LOGGER = LogManager.getLogger(TagDaoImpl.class);
    private static SessionFactory sessionFactory;

    public TagDaoImpl(SessionFactory sessionFactory) {
        LOGGER.info("TagDaoImpl initialized");
        LOGGER.info("sessionFactory is null " + (sessionFactory == null));
        TagDaoImpl.sessionFactory = sessionFactory;
    }

    @Override
    public Tag getTagById(int tagId) {
        LOGGER.info("getTagById() started");
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Tag tag = session.get(Tag.class, tagId);

        transaction.commit();
        session.close();
        return tag;
    }

    @Override
    public int saveTag(Tag tag) {
        LOGGER.info("saveTag() started");

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        LOGGER.info("tag to save = " + tag);
        session.save(tag);

        transaction.commit();
        session.close();
        return 1;
    }

    @Override
    public void saveListOfTags(Set<Tag> tagSet) {
        LOGGER.info("saveListOfTags() started");

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        LOGGER.info("tagSet to save = " + tagSet);
        Tag tag;
        for (Iterator<Tag> tagIterator = tagSet.iterator(); tagIterator.hasNext(); ) {
            tag = tagIterator.next();
            if (null == getTagByTitle(tag.getTitle()).getTitle()) {
                session.save(tag);
            }
        }
        transaction.commit();
        session.close();
    }

    @Override
    public Tag getTagByTitle(String title) {
        LOGGER.info("getTagByTitle() started");

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        LOGGER.info("title to getTag = " + title);
        String queryText = " from tag where title like '%" + title + "%'";
        Query query = session.createQuery(queryText);
        Tag tag = new Tag();
        List<Tag> urlList = query.list();
        transaction.commit();
        session.close();
        LOGGER.info("getTagByTitle returned = " + urlList);
        if (urlList.size() > 0) {
            tag = urlList.get(0);
        }
        LOGGER.info("getTagByTitle() got tag from result = " + tag);

        return tag;
    }
}
