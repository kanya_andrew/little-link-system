package com.littleLink.dao;

public interface DaoFactory {

    SiteUserDao getSiteUserDao();

    TagDao getTagDao();

    UrlDao getUrlDao();
}
