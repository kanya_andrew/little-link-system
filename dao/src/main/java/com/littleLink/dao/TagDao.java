package com.littleLink.dao;

import com.littleLink.model.Tag;

import java.util.Set;

public interface TagDao {

    Tag getTagById(int tagId);

    int saveTag(Tag tag);

    void saveListOfTags(Set<Tag> tagSet);

    Tag getTagByTitle(String title);
}
