package com.littleLink.dao;

import com.littleLink.model.SiteUser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.FileInputStream;
import java.util.List;

public class SiteUserDaoImplTest extends HibernateDbUnitAbstractTestCase {
    private static final Logger LOGGER = LogManager.getLogger(SiteUserDaoImplTest.class);
    private static final String DEFAULT_DATA_XML_PATH = "src\\test\\resources\\siteUserDataSet.xml";
    private static final int SITE_USER_TO_TEST_ID1 = 1;


    @Autowired
    SiteUserDao siteUserDao;

    @Test
    public void testGetSiteUserById() {

        SiteUser expectedSiteUser = session.get(SiteUser.class, SITE_USER_TO_TEST_ID1);
        LOGGER.info("expectedSiteUser = " + expectedSiteUser);

        SiteUser actualSiteUser = siteUserDao.getSiteUserById(SITE_USER_TO_TEST_ID1);
        LOGGER.info("actualSiteUser = " + actualSiteUser);

        Assert.assertEquals(expectedSiteUser, actualSiteUser);
    }

    @Test
    public void testSaveSiteUser() {

        SiteUser expectedSiteUser = createSiteUserToTest();
        LOGGER.info("SiteUserToSave = " + expectedSiteUser);

        siteUserDao.saveSiteUser(expectedSiteUser);

        String hql = "SELECT MAX(id) from site_user";

        session.beginTransaction();
        List list = session.createQuery(hql).list();
        int maxId = ((Integer) list.get(0)).intValue();
        LOGGER.info("maxId = " + maxId);

        SiteUser actualSiteUser;
        actualSiteUser = session.get(SiteUser.class, maxId);
        LOGGER.info("actualSiteUser = " + actualSiteUser);

        expectedSiteUser.setId(maxId);
        LOGGER.info("expectedSiteUser = " + actualSiteUser);

        session.delete(actualSiteUser);
        session.getTransaction().commit();

        Assert.assertEquals(expectedSiteUser, actualSiteUser);
    }

    private SiteUser createSiteUserToTest() {
        LOGGER.info("createSiteUserToTest started");
        SiteUser siteUser = new SiteUser();
        siteUser.setLogin("ann");
        siteUser.setPassword("pass");
        siteUser.setEmail("mail");
        siteUser.setDeleted(false);
        LOGGER.info("created new site user " + siteUser);
        return siteUser;
    }

    protected IDataSet getDataSet() throws Exception {
        return new FlatXmlDataSetBuilder().build(new FileInputStream(DEFAULT_DATA_XML_PATH));
    }

}