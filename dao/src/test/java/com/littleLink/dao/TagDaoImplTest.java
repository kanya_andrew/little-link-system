package com.littleLink.dao;

import com.littleLink.dao.impl.TagDaoImpl;
import com.littleLink.model.Tag;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.FileInputStream;
import java.util.List;

public class TagDaoImplTest extends HibernateDbUnitAbstractTestCase {
    private static final Logger LOGGER = LogManager.getLogger(TagDaoImplTest.class);
    private static final String DEFAULT_DATA_XML_PATH = "src\\test\\resources\\tagDataSet.xml";
    private static final int TAG_TO_TEST_ID1 = 1;

    @Autowired
    TagDao tagDao;

    @Test
    public void testGetTagById() {

        Tag expectedTag = session.get(Tag.class, TAG_TO_TEST_ID1);
        LOGGER.info("expectedTag = " + expectedTag);

        Tag actualTag = tagDao.getTagById(TAG_TO_TEST_ID1);
        LOGGER.info("actualTag = " + actualTag);

        Assert.assertEquals(expectedTag, actualTag);
    }

    @Test
    public void testSaveTag() {

        Tag expectedTag = createTagToTest();
        LOGGER.info("TagToSave = " + expectedTag);

        tagDao.saveTag(expectedTag);

        String hql = "SELECT MAX(id) from tag";

        session.beginTransaction();
        List list = session.createQuery(hql).list();
        int maxId = ((Integer) list.get(0)).intValue();
        LOGGER.info("maxId = " + maxId);

        Tag actualTag;
        actualTag = session.get(Tag.class, maxId);
        LOGGER.info("actualTag = " + actualTag);

        expectedTag.setId(maxId);
        LOGGER.info("expectedTag = " + actualTag);

        session.delete(actualTag);
        session.getTransaction().commit();

        Assert.assertEquals(expectedTag, actualTag);
    }

    private Tag createTagToTest() {
        Tag tag = new Tag();
        tag.setTitle("google");
        tag.setCounter(1);
        return tag;
    }

    protected IDataSet getDataSet() throws Exception {
        return new FlatXmlDataSetBuilder().build(new FileInputStream(DEFAULT_DATA_XML_PATH));
    }

}