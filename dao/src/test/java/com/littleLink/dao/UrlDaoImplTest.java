package com.littleLink.dao;

import com.littleLink.model.SiteUser;
import com.littleLink.model.Url;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.FileInputStream;
import java.util.List;

public class UrlDaoImplTest extends HibernateDbUnitAbstractTestCase{

    private static final Logger LOGGER = LogManager.getLogger(UrlDaoImplTest.class);
    private static final String DEFAULT_DATA_XML_PATH = "src\\test\\resources\\urlDataSet.xml";
    private static final String REDUCED_URL_TO_TEST = "xt5jds";
    private static final int URL_TO_TEST_ID1 = 1;
    private static final int SITE_USER_TO_TEST_ID1 = 1;

    @Autowired
    UrlDao urlDao;

    @Autowired
    SiteUserDao siteUserDao;

    @Test
    public void testGetUrlById() {

        Url expectedUrl = session.get(Url.class, URL_TO_TEST_ID1);
        LOGGER.info("expectedUrl = " + expectedUrl);

        Url actualUrl = urlDao.getUrlById(URL_TO_TEST_ID1);
        LOGGER.info("actualUrl = " + actualUrl);

        Assert.assertEquals(expectedUrl, actualUrl);
    }

    @Test
    public void testSaveUrl() {

        Url expectedUrl = createUrlToTest();
        LOGGER.info("UrlToSave = " + expectedUrl);

        urlDao.saveUrl(expectedUrl);

        String hql = "SELECT MAX(id) from url";

        session.beginTransaction();
        List list = session.createQuery(hql).list();
        int maxId = ((Integer) list.get(0)).intValue();
        LOGGER.info("maxId = " + maxId);

        Url actualUrl;
        actualUrl = session.get(Url.class, maxId);
        LOGGER.info("actualUrl = " + actualUrl);

        expectedUrl.setId(maxId);
        LOGGER.info("expectedUrl = " + actualUrl);

        session.delete(actualUrl);
        session.getTransaction().commit();

        Assert.assertEquals(expectedUrl, actualUrl);
    }

    @Test
    public void testGetUrlByReducedUrl(){
        Url expectedUrl = urlDao.getUrlById(URL_TO_TEST_ID1);
        Url actualUrl = urlDao.getUrlByReducedUrl(REDUCED_URL_TO_TEST);

        Assert.assertEquals(expectedUrl, actualUrl);
    }

    private Url createUrlToTest() {
        Url url = new Url();
        url.setTitle("bitbucket");
        url.setReducedPath("https://bitbucket.org/kanya_andrew/little-link-system/commits/all");
        url.setFullPath("xxx");
        url.setDescription("");
        SiteUser siteUser = siteUserDao.getSiteUserById(SITE_USER_TO_TEST_ID1);
        url.setAddedByUser(siteUser);
        url.setCounter(1);
        return url;
    }

    protected IDataSet getDataSet() throws Exception {
        return new FlatXmlDataSetBuilder().build(new FileInputStream(DEFAULT_DATA_XML_PATH));
    }
}