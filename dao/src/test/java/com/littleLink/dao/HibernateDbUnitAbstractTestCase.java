package com.littleLink.dao;

import org.apache.commons.lang.NotImplementedException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.dbunit.DBTestCase;
import org.dbunit.PropertiesBasedJdbcDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.sql.DataSource;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContext-test.xml")
public abstract class HibernateDbUnitAbstractTestCase extends DBTestCase {

    private static final Logger LOGGER = LogManager.getLogger(HibernateDbUnitAbstractTestCase.class);

    protected Session session;

    @Autowired
    DataSource dataSource;

    @Autowired
    SessionFactory sessionFactory;

    @Value("${currentSchema}")
    public String currentSchema;

    @Value("${jdbc.url}")
    public String jdbcUrl;

    @Value("${jdbc.user}")
    public String jdbcUser;

    @Value("${jdbc.pass}")
    public String jdbcPass;

    @Value("${jdbc.driverClassName}")
    public String jdbcDriverClassName;

    @Before
    public void setUp() throws Exception {
        LOGGER.info("setUp run");
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_DRIVER_CLASS, jdbcDriverClassName);
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_CONNECTION_URL, jdbcUrl);
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_USERNAME, jdbcUser);
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_PASSWORD, jdbcPass);
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_SCHEMA, currentSchema);

        LOGGER.info("sessionFactory is null " + (sessionFactory == null));
        session = sessionFactory.openSession();
        super.setUp();
        LOGGER.info("setUp finished");
    }

    @After
    public void tearDown() throws Exception {
        session.close();
        super.tearDown();
    }

    /**
     * {@inheritDoc}
     */
    protected IDataSet getDataSet() throws Exception {
        throw new NotImplementedException("Specify data set for test: " + this.getClass().getSimpleName());
    }

    /**
     * {@inheritDoc}
     */
    protected DatabaseOperation getSetUpOperation() throws Exception {
        return DatabaseOperation.REFRESH;
    }

    /**
     * {@inheritDoc}
     */
    protected DatabaseOperation getTearDownOperation() throws Exception {
        return DatabaseOperation.NONE;
    }

    public SessionFactory getSessionFactory() {
        LOGGER.info("sessionFactory is null = " + (sessionFactory == null));
        return sessionFactory;
    }

    public DataSource getDataSource() {
        return dataSource;
    }
}