package com.littleLink.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "tag")
public class Tag implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "tag_generator")
    @SequenceGenerator(initialValue = 100, allocationSize = 1, name = "tag_generator", sequenceName = "tag_sequence")
    @Column(name = "tag_id")
    private int id;

    @Column(name = "title", unique=true)
    private String title;

    @Column(name = "counter")
    private int counter;

    @ManyToMany(mappedBy = "tagList")
    private List<Url> urlList = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tag tag = (Tag) o;

        if (id != tag.id) return false;
        if (counter != tag.counter) return false;
        return title.equals(tag.title);

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + title.hashCode();
        result = 31 * result + counter;
        return result;
    }

    @Override
    public String toString() {
        return "Tag{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", counter=" + counter +
                '}';
    }
}
