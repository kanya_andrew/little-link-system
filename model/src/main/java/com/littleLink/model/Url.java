package com.littleLink.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity(name = "url")
public class Url implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "url_generator")
    @SequenceGenerator(initialValue = 100, allocationSize = 1, name = "url_generator", sequenceName = "url_sequence")
    @Column(name = "url_id")
    private int id;

    @Column(name = "title")
    private String title;

    @Column(name = "reduced_path")
    private String reducedPath;

    @Column(name = "full_path")
    private String fullPath;

    @Column(name = "description")
    private String description;

    @Column(name = "counter")
    private int counter;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private SiteUser addedByUser;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<Tag> tagList = new HashSet<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getReducedPath() {
        return reducedPath;
    }

    public void setReducedPath(String reducedPath) {
        this.reducedPath = reducedPath;
    }

    public String getFullPath() {
        return fullPath;
    }

    public void setFullPath(String fullPath) {
        this.fullPath = fullPath;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public Set<Tag> getTagList() {
        return tagList;
    }

    public void setTagList(Set<Tag> tagList) {
        this.tagList = tagList;
    }

    public SiteUser getAddedByUser() {
        return addedByUser;
    }

    public void setAddedByUser(SiteUser addedByUser) {
        this.addedByUser = addedByUser;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Url url = (Url) o;

        if (id != url.id) return false;
        if (counter != url.counter) return false;
        if (!title.equals(url.title)) return false;
        if (!reducedPath.equals(url.reducedPath)) return false;
        if (!fullPath.equals(url.fullPath)) return false;
        return description != null ? description.equals(url.description) : url.description == null;

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + title.hashCode();
        result = 31 * result + reducedPath.hashCode();
        result = 31 * result + fullPath.hashCode();
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + counter;
        return result;
    }

    @Override
    public String toString() {
        return "Url{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", reducedPath='" + reducedPath + '\'' +
                ", fullPath='" + fullPath + '\'' +
                ", description='" + description + '\'' +
                ", counter='" + counter + '\'' +
                '}';
    }
}
