package com.littleLink.controllerMvc;

import com.littleLink.model.SiteUser;
import com.littleLink.model.Url;
import com.littleLink.service.RegistrationService;
import com.littleLink.service.SiteUserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import java.io.IOException;

@RestController
public class RegistrationController {

    private static final Logger LOGGER = LogManager.getLogger(RegistrationController.class);

    @Autowired
    SiteUserService siteUserService;

    @Autowired
    RegistrationService registrationService;

    @RequestMapping(value = "/registration")
    ModelAndView viewRegistrationPage() throws ServletException, IOException {
        LOGGER.info("viewRegistrationPage() run");
        SiteUser siteUser = new SiteUser();
        ModelAndView modelAndView = new ModelAndView("/registration");
        modelAndView.getModelMap().addAttribute("user", siteUser);

        return modelAndView;
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    ModelAndView registrateUser(@ModelAttribute("user") SiteUser siteUser, BindingResult result) throws ServletException, IOException {
        LOGGER.info("registrateUser() run");

        if (!result.hasErrors()) {
            registrationService.bookNewUserAccount(siteUser);
            return new ModelAndView("/login" ,"url", new Url());
        } else {
            return new ModelAndView("registration", "user", siteUser);
        }
    }
}
