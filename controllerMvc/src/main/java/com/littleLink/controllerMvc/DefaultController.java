package com.littleLink.controllerMvc;

import com.littleLink.model.Url;
import com.littleLink.service.UrlService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
public class DefaultController {

    private static final Logger LOGGER = LogManager.getLogger(DefaultController.class);

    @Autowired
    UrlService urlService;

    @RequestMapping(value = "/")
    ModelAndView viewIndexPage() throws ServletException, IOException {

        LOGGER.info("viewIndexPage run");
        return new ModelAndView("index");
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public ModelAndView logoutPage(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return new ModelAndView("/index");
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{shortUrl}")
    ModelAndView viewFullUrlPage(@PathVariable String shortUrl) throws ServletException, IOException {
        LOGGER.info("viewFullUrlPage run");
        Url url = urlService.getUrlByReducedUrl(shortUrl);
        LOGGER.info("got url " + url);
        ModelAndView modelAndView = new ModelAndView();
        if (url.getId() == 0) {
            modelAndView.setViewName(shortUrl);
        } else {
            url.setCounter(url.getCounter() + 1);
            urlService.updateUrl(url);
            modelAndView.setViewName("redirect:" + url.getFullPath());
        }
        LOGGER.info("viewFullUrlPage ViewName" + modelAndView.getViewName());

        return modelAndView;
    }

}
