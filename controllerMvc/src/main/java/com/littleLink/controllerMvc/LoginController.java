package com.littleLink.controllerMvc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import java.io.IOException;

@RestController
public class LoginController {

    private static final Logger LOGGER = LogManager.getLogger(LoginController.class);

    @RequestMapping( value = "/login")
    ModelAndView viewLoginPage() throws ServletException, IOException {

        LOGGER.info("viewLoginPage run");
        return new ModelAndView("/login");
    }

    @RequestMapping( value = "/404")
    ModelAndView viewErrorPage() throws ServletException, IOException {

        LOGGER.info("viewErrorPage run");
        return new ModelAndView("/404");
    }
}
