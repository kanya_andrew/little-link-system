package com.littleLink.controllerMvc;

import com.littleLink.model.SiteUser;
import com.littleLink.model.Tag;
import com.littleLink.model.Url;
import com.littleLink.service.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
public class DashboardController {

    private static final Logger LOGGER = LogManager.getLogger(DashboardController.class);

    @Autowired
    TagExtractionService tagExtractionService;

    @Autowired
    UrlService urlService;

    @Autowired
    ReducePathGeneratorService reducePathGeneratorService;

    @Autowired
    TagService tagService;

    @Autowired
    SiteUserService siteUserService;

    @RequestMapping(value = "/dashboard")
    ModelAndView viewDashboardPage() throws ServletException, IOException {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String userName = auth.getName();

        SiteUser siteUser = siteUserService.getSiteUserByLogin(userName);

        LOGGER.info("viewDashboardPage run");
        Url url = new Url();
        ModelAndView modelAndView = new ModelAndView("/dashboard");
        ModelMap modelMap = modelAndView.getModelMap();
        List<Url> urlList = urlService.getUserUrlList(siteUser);
        modelMap.addAttribute("urlList",urlList);
        modelMap.addAttribute("url", url);

        return modelAndView;
    }

    @RequestMapping(value = "/dashboard" , method = RequestMethod.POST)
    ModelAndView createUrl(@ModelAttribute("user") Url url, BindingResult result, WebRequest request) throws ServletException, IOException {
        LOGGER.info("createLink run");

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String userName = auth.getName();

        SiteUser siteUser = siteUserService.getSiteUserByLogin(userName);
        String scrollOfTags = request.getParameter("tags");
        if (!result.hasErrors()) {
            Set<Tag> tagSet = tagExtractionService.extractTagsFromString(scrollOfTags);
            tagService.saveListOfTags(tagSet);

            Set<Tag> tagSetWithId = new HashSet<>();
            for (Tag tag :tagSet) {
                tagSetWithId.add(tagService.getTagByTitle(tag.getTitle()));
            }

            url.setTagList(tagSetWithId);
            url.setAddedByUser(siteUser);
            url.setReducedPath(reducePathGeneratorService.generateReducedPath());
            urlService.saveUrl(url);

            ModelAndView modelAndView = new ModelAndView("/dashboard");
            ModelMap modelMap = modelAndView.getModelMap();
            List<Url> urlList = urlService.getUserUrlList(siteUser);
            modelMap.addAttribute("urlList",urlList);
            modelMap.addAttribute(new Url());
            return modelAndView;
        } else {
            return new ModelAndView("/dashboard", "url", url);
        }
    }



}
