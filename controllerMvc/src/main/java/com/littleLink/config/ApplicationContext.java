package com.littleLink.config;

import com.littleLink.dao.DaoFactory;
import com.littleLink.dao.SiteUserDao;
import com.littleLink.dao.TagDao;
import com.littleLink.dao.UrlDao;
import com.littleLink.dao.impl.DaoFactoryImpl;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;


@Configuration
@ComponentScan(basePackages = {"com.littleLink.**"})
@ImportResource("classpath:/applicationContext.xml")
public class ApplicationContext extends WebMvcConfigurerAdapter {

    private static final Logger LOGGER = LogManager.getLogger(ApplicationContext.class);

    @Autowired
    BasicDataSource dataSource;

    @Bean
    public InternalResourceViewResolver viewResolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/pages/");
        resolver.setSuffix(".jsp");
        return resolver;
    }

    @Bean
    public ApplicationContextProvider applicationContextProvider() {
        LOGGER.info("ApplicationContextProvider bean is initialised");
        return new ApplicationContextProvider();
    }

    @Bean
    public DaoFactory daoFactory() {
        LOGGER.info("daoFactory bean is initialised");
        DaoFactoryImpl daoFactoryImpl = new DaoFactoryImpl();
        return daoFactoryImpl;
    }

    @Bean
    public UrlDao urlDao() {
        return daoFactory().getUrlDao();
    }

    @Bean
    public SiteUserDao siteUserDao() {
        return daoFactory().getSiteUserDao();
    }

    @Bean
    public TagDao tagDao() {
        return daoFactory().getTagDao();
    }


}
