<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><spring:message code="label.form.titleUrl"></spring:message></title>
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/main.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/js/jquery-1.12.1.min.js" />"></script>
    <script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
</head>
<body>
<div class="container col-sm-12 col-sm-offset-0 col-md-6 col-md-offset-4 col-lg-4 col-lg-offset-4">
    <div class="row">
        <nav class="navbar navbar-default col-12">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="/"><spring:message code="label.form.siteTitle"></spring:message></a>
                </div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="/">Home</a></li>
                    <li><a href="<c:url value="/dashboard" />">
                        <spring:message code="label.form.dashboardLink"></spring:message>
                    </a></li>
                </ul>
            </div>
        </nav>

        <div class="col-sm-12">
            <form action="/login" class=""
                  method="post">

                <label><b><spring:message code="label.form.username"></spring:message></b></label>
                <input type="text" name="username" class="form-control" placeholder=<spring:message code="label.form.enterUsername"></spring:message> required>

                <label><b><spring:message code="label.form.password"></spring:message></b></label>
                <input type="password" class="form-control" placeholder=<spring:message code="label.form.enterPassword"></spring:message> name="password" required>

                <input type="submit" class="form-control btn-success" name="submit" value=<spring:message
                        code="label.form.loginButoon"></spring:message>></button>

                <input type="hidden"
                       name="${_csrf.parameterName}"
                       value="${_csrf.token}"/>
            </form>
        </div>
        <div class="col-sm-12 text-center">
            <a href="<c:url value="/registration" />">
                <spring:message code="label.form.registrationLink"></spring:message>
            </a>
        </div>
        <div class="imageContainer   col-sm-6 col-sm-offset-2">
            <img src="${pageContext.request.contextPath}/resources/img/avatar.png" alt="Avatar" class="avatar">
        </div>
    </div>
</div>
</body>
</html>
