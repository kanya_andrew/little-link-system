<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><spring:message code="label.form.titleIndex"></spring:message></title>
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/main.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/js/jquery-1.12.1.min.js" />"></script>
    <script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
</head>
<body>
<div class="container col-sm-12 col-sm-offset-0 col-md-6 col-md-offset-4 col-lg-4 col-lg-offset-4">
    <div class="row">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="/"><spring:message code="label.form.siteTitle"></spring:message></a>
                </div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="/">Home</a></li>
                    <li><a href="<c:url value="/dashboard" />">
                        <spring:message code="label.form.dashboardLink"></spring:message>
                    </a></li>
                </ul>
            </div>
        </nav>
        <div class="col-sm-12">
            <a href="<c:url value="/logout"/>" class="btn btn-danger col-sm-offset-4 col-sm-4"
               role="button"><spring:message
                    code="label.form.logoutButton"> </spring:message></a>
        </div>

        <div class="form-group">
            <div class="col-sm-12">
                <form action="/dashboard" class=""
                      method="post">

                    <label><b> <spring:message code="label.form.linkTitle"></spring:message></b></label>
                    <form:input path="url.title" class="form-control" value=""/>
                    <form:errors path="title" element="div"/>


                    <label><b> <spring:message code="label.form.linkDesription"></spring:message></b></label>
                    <form:input path="url.description" class="form-control" value=""/>
                    <form:errors path="description" element="div"/>

                    <label><b><spring:message code="label.form.fullLink"></spring:message></b></label>
                    <form:input path="url.fullPath" class="form-control" value=""/>
                    <form:errors path="fullPath" element="div"/>

                    <label><b><spring:message code="label.form.tags"></spring:message></b></label>
                    <input type="text" name="tags" class="form-control" placeholder=
                    <spring:message code="label.form.enterTags"></spring:message> required>

                    <input type="hidden"
                           name="${_csrf.parameterName}"
                           value="${_csrf.token}"/>

                    <input type="submit" class="form-control btn-success" name="submit" value=<spring:message
                            code="label.form.submit"></spring:message>></button>
                </form>
            </div>
        </div>
        <div class="col-sm-12">
            <table class="table">
                <thead>
                <tr>
                    <th><spring:message code="label.table.linkTitle"></spring:message></th>
                    <th><spring:message code="label.table.reducedPath"></spring:message></th>
                    <th><spring:message code="label.table.linkCounter"></spring:message></th>
                    <th><spring:message code="label.table.tags"></spring:message></th>
                </tr>
                </thead>
                <tbody>

                <c:forEach items="${urlList}" var="url">
                    <tr>
                        <td>${url.title}</td>
                        <td class='text-center'><a href="${url.reducedPath}"><spring:message
                                code="label.table.domain"></spring:message>${url.reducedPath}</a></td>
                        <td class='text-center'>${url.counter}</td>
                        <td class='text-center'>
                            <c:forEach items="${url.tagList}" var="tag">
                                ${tag.title}
                            </c:forEach></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <div class="imageContainer  col-sm-6 col-sm-offset-2">
            <img src="${pageContext.request.contextPath}/resources/img/avatar.png" alt="Avatar" class="avatar ">
        </div>
    </div>
</div>
</body>
</html>
