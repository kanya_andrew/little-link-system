<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page session="false" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
    <title><spring:message code="label.form.titleRegistration"></spring:message></title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css"/>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery-1.12.1.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container col-sm-12 col-sm-offset-0 col-md-6 col-md-offset-4 col-lg-4 col-lg-offset-4">
    <div class="row">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="/"><spring:message code="label.form.siteTitle"></spring:message></a>
                </div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="/">Home</a></li>
                    <li><a href="<c:url value="/dashboard" />">
                        <spring:message code="label.form.dashboardLink"></spring:message>
                    </a></li>
                </ul>
            </div>
        </nav>

        <div class="form-group">
            <div class="col-sm-12">
                <spring:message code="label.form.titleRegistration"></spring:message>
                <form action="/registration" class=""
                      method="post">

                    <label><b> <spring:message code="label.user.login"></spring:message></b></label>
                    <form:input path="user.login" class="form-control" value=""/>
                    <form:errors path="login" element="div"/>

                    <label><b>
                        <spring:message code="label.user.password"></spring:message></b></label>
                    <form:input path="user.password" value="" class="form-control" type="password"/>
                    <form:errors path="password" element="div"/>

                    <label><b><spring:message code="label.user.email"></spring:message></b></label>
                    <form:input path="user.email" class="form-control" value=""/>
                    <form:errors path="email" element="div"/>

                    <input type="hidden"
                           name="${_csrf.parameterName}"
                           value="${_csrf.token}"/>

                    <input type="submit" class="form-control btn-success" name="submit" value=<spring:message
                            code="label.form.submit"></spring:message>></button>
                </form>
            </div>

        </div>
        <div class="col-sm-12 text-center">
            <a href="<c:url value="/login" />">
                <spring:message code="label.form.loginLink"></spring:message>
            </a>
        </div>
        <div class="imageContainer  col-sm-6 col-sm-offset-2">
            <img src="${pageContext.request.contextPath}/resources/img/avatar.png" alt="Avatar" class="avatar ">
        </div>

    </div>
</div>

</body>
</html>